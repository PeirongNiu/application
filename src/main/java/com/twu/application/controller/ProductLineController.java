package com.twu.application.controller;

import com.twu.application.entity.ProductLine;
import com.twu.application.service.ProductLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductLineController {
    @Autowired
    private ProductLineService productLineService;

    @GetMapping("/product-line")
    public ResponseEntity getAllProductLine() {
        List<ProductLine> allProductLine = productLineService.getAllProductLine();
        return ResponseEntity.status(200).body(allProductLine);
    }

}
