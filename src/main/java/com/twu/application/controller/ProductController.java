package com.twu.application.controller;

import com.twu.application.entity.Product;
import com.twu.application.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/product")
    public ResponseEntity getAllProduct(){
        List<Product> allProduct = productService.getAllProduct();
        return ResponseEntity.status(200).body(allProduct);
    }
}
