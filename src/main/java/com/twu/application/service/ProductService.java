package com.twu.application.service;

import com.twu.application.dao.ProductRepository;
import com.twu.application.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProduct() {

        return productRepository.findAll();
    }
}
