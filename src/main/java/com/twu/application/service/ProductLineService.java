package com.twu.application.service;

import com.twu.application.dao.ProductLineRepository;
import com.twu.application.entity.ProductLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductLineService {

    @Autowired
    private ProductLineRepository productLineRepository;

    public List<ProductLine> getAllProductLine() {
        return productLineRepository.findAll();
    }
}
